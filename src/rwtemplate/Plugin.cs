﻿using BepInEx;
using RWCustom;
using RWTEMPLATE.Core;
using System;
using System.Collections.Concurrent;
using System.IO;

namespace RWTEMPLATE
{
    [BepInPlugin(PLUGIN_GUID, PLUGIN_NAME, PLUGIN_VERSION)]
    public class Plugin : BaseUnityPlugin
    {
        public const string PLUGIN_GUID = "rwtemplate";
        public const string PLUGIN_NAME = "Rain World Template Mod";
        public const string PLUGIN_VERSION = "1.0.0";

        private void Awake()
        {
            try
            {
                Globals.mls = Logger;
                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loading!");

                // code

                Logger.LogInfo($"Plugin {PLUGIN_NAME} is loaded!");
            }
            catch (Exception ex)
            {
                Logger.LogError(ex.ToString());
            }
        }
    }
}
