# rain-world-template-mod


## What it is

This is just a barebones coding template for rain world mods which should work
for recent (post downpour) versions of rain world.

## Features

- Preconfigured .NET 4.8 w/ x86 target.
- Integrated BepInEx nuget feed.
- Installed BepInEx nuget references.
- Pre-referenced rain world game assemblies (they will become invalid for you, you need to re-add them)

Namely:
|Name
|-
|Rain World\RainWorld_Data\Managed\UnityModule*
|Rain World\BepInEx\plugins\HOOKS-Assembly-CSharp.dll
|Rain World\BepInEx\plugins\utils\PUBLIC-Assembly-CSharp.dll

## Missing references

This project includes a script to automatically fix missing references added via HintPath in the csproj files.
Just run "update-hintpaths-auto.ps1" and make sure the gameinstalldir.txt contains your correct path to Rain World.

You might want to do this before trying to open the project or compiling.

## Building

The solution features a powershell script that copies the binary output libs to mod_output and then to the LOCAL game mods folder.
Make sure to check the gameinstalldir.txt file and put your game path inside it for this pipeline to work.

Feel free to change the powershell script (needed if you add external library references to your project, such as SQLite or something for example)
If you don't want to use it, just remove it from the post-build process in the solutions csproj settings